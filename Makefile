INSTALL_PATH = ~/.local/share/gnome-shell/extensions
INSTALL_NAME = gnome-fuzzy-app-search@gnome-shell-extensions.Czarlie.gitlab.com
BUILD_DIR = _build
FILES = metadata.json\
		README.md\
		applicationsSearchProvider.js\
		applicationsUtils.js\
		extension.js\
		fileUtils.js\
		indexUtils.js\
		metadata.js\
		providers.js\
		scorer.js\
		tokenizer.js\
		stylesheet.css
# 		xkbUtils

install: build
	rm -rf $(INSTALL_PATH)/$(INSTALL_NAME)
	mkdir -p $(INSTALL_PATH)/$(INSTALL_NAME)
	cp -r --preserve=timestamps $(BUILD_DIR)/* $(INSTALL_PATH)/$(INSTALL_NAME)
	rm -rf $(BUILD_DIR)
	echo Installed in $(INSTALL_PATH)/$(INSTALL_NAME)

archive: build
	cd ${BUILD_DIR} && zip -r ../archive.zip *
	rm -rf $(BUILD_DIR)
	echo Archive created

build:
	rm -rf $(BUILD_DIR)
	mkdir $(BUILD_DIR)
	cp -r --preserve=timestamps $(FILES) $(BUILD_DIR)
	echo Build was successful

clean:
	rm -rf $(BUILD_DIR)

uninstall:
	rm -rf $(INSTALL_PATH)/$(INSTALL_NAME)
